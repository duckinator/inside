require "inside/version"

module Inside
  def inside(obj, &block)
    obj.module_eval(&block)
  end
end

include Inside
